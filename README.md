Template boilerplate
====================

**Grunt CLI** - http://gruntjs.com/getting-started

**Instalace Grunt tasků**

`$ npm install`

**Spuštění Grunt watcheru**

* kompilace SCSS
* transpilace JavaScriptu z ES6 do ES5.1 a umožnění modulárního JavaScriptu pomocí Babelu a Browserify
* uglify JavaScriptu

`$ grunt`