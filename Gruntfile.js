module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
    },

    uglify: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'js/dist/app.min.js': 'js/dist/app.js'
        }
      }
    },

    browserify: {
      dist: {
        options: {
          transform: [['babelify', {presets: ['env']}]]
        },
        src: ['js/app.js'],        // zdroj napsaný v ES 6
        dest: 'js/dist/app.js'        // cílový soubor transpilovaný do ES 5.1
      }
    },

    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass']
      },

      uglifyJs: {
        files: 'js/dist/app.js',
        tasks: ['uglify']
      },

      js: {
        files: ['js/*.js', 'js/*/*.js', '!js/*.min.js', '!js/dist/*','!**/node_modules/**'],
        tasks: ['browserify']
      }
    }

  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
};